/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <stdint.h>
#include <ctype.h>

#include <amxc/amxc.h>
#include <amxc/amxc_macros.h>
#include <amxs/amxs.h>
#include <amxm/amxm.h>
#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>
#include <netmodel/client.h>
#include <netmodel/mib.h>
#include "mib_ppp.h"

#define ME "ppp_mib"
#define MOD "ppp"
#define string_empty(x) ((x == NULL) || (*x == '\0'))

static amxc_var_t ppp_names;

static void update_flag(const char* netmodel_intf, const char* flag, bool set) {
    amxc_var_t args;
    amxc_var_t ret;
    const char* function = set ? "setFlag" : "clearFlag";

    amxc_var_init(&args);
    amxc_var_init(&ret);

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, "flag", flag);

    if(AMXB_STATUS_OK != amxb_call(netmodel_get_amxb_bus(), netmodel_intf, function, &args, &ret, 5)) {
        SAH_TRACEZ_ERROR(ME, "Failed to call %s%s(flag='%s')", netmodel_intf, function, flag);
        goto exit;
    }

exit:
    amxc_var_clean(&args);
    amxc_var_clean(&ret);
}

static amxs_status_t status_action_cb(const amxs_sync_entry_t* sync_entry UNUSED,
                                      amxs_sync_direction_t direction,
                                      amxc_var_t* data,
                                      void* priv UNUSED) {
    const char* netmodel_intf = GET_CHAR(data, "path");
    const char* status = GETP_CHAR(data, "parameters.Status");
    bool is_up = false;

    when_false((direction == amxs_sync_a_to_b), exit);

    is_up = !string_empty(status) && (strcmp(status, "Up") == 0);

    update_flag(netmodel_intf, "ppp-up", is_up);
exit:
    return amxs_status_ok;
}

static amxs_status_t add_parameters(amxs_sync_ctx_t* ctx) {
    amxs_status_t status = amxs_status_unknown_error;
    amxs_sync_object_t* obj = NULL;

    status = amxs_sync_ctx_add_new_copy_param(ctx, "ConnectionStatus", "ConnectionStatus", AMXS_SYNC_DEFAULT);
    status |= amxs_sync_ctx_add_new_copy_param(ctx, "LastConnectionError", "LastConnectionError", AMXS_SYNC_DEFAULT);
    status |= amxs_sync_ctx_add_new_copy_param(ctx, "Username", "Username", AMXS_SYNC_DEFAULT);
    status |= amxs_sync_ctx_add_new_copy_param(ctx, "Password", "Password", AMXS_SYNC_DEFAULT);
    status |= amxs_sync_ctx_add_new_param(ctx, "Status", "Status", AMXS_SYNC_DEFAULT, amxs_sync_param_copy_trans_cb, status_action_cb, ctx);

    status |= amxs_sync_object_new_copy(&obj, "IPCP.", ".", AMXS_SYNC_DEFAULT);
    status |= amxs_sync_object_add_new_copy_param(obj, "LocalIPAddress", "LocalIPAddress", AMXS_SYNC_DEFAULT);
    status |= amxs_sync_object_add_new_copy_param(obj, "RemoteIPAddress", "RemoteIPAddress", AMXS_SYNC_DEFAULT);
    status |= amxs_sync_object_add_new_copy_param(obj, "DNSServers", "DNSServers", AMXS_SYNC_DEFAULT);
    status |= amxs_sync_ctx_add_object(ctx, obj);

    status |= amxs_sync_object_new_copy(&obj, "IPv6CP.", ".", AMXS_SYNC_DEFAULT);
    status |= amxs_sync_object_add_new_copy_param(obj, "LocalInterfaceIdentifier", "LocalInterfaceIdentifier", AMXS_SYNC_DEFAULT);
    status |= amxs_sync_object_add_new_copy_param(obj, "RemoteInterfaceIdentifier", "RemoteInterfaceIdentifier", AMXS_SYNC_DEFAULT);
    status |= amxs_sync_ctx_add_object(ctx, obj);

    return status;
}

static netmodel_mib_t mib = {
    .name = ME,
    .root = "PPP.",
    .add_sync_parameters = add_parameters,
};

int mib_added(UNUSED const char* const function_name,
              amxc_var_t* args,
              UNUSED amxc_var_t* ret) {
    netmodel_subscribe(args, &mib);

    return 0;
}

int mib_removed(UNUSED const char* const function_name,
                amxc_var_t* args,
                UNUSED amxc_var_t* ret) {
    netmodel_unsubscribe(args, &mib);
    update_flag(GET_CHAR(args, "object"), "ppp-up", false);

    return 0;
}

static AMXM_CONSTRUCTOR mib_init(void) {
    amxm_shared_object_t* so = amxm_so_get_current();
    amxm_module_t* mod = NULL;

    SAH_TRACEZ_INFO(ME, "Mib init triggered");

    amxm_module_register(&mod, so, MOD);
    amxm_module_add_function(mod, "mib-added", mib_added);
    amxm_module_add_function(mod, "mib-removed", mib_removed);

    netmodel_initialize();

    amxc_var_init(&ppp_names);
    amxc_var_set_type(&ppp_names, AMXC_VAR_ID_HTABLE);

    return 0;
}

static AMXM_DESTRUCTOR mib_clean(void) {
    SAH_TRACEZ_INFO(ME, "Mib clean triggered");

    netmodel_cleanup();

    amxc_var_clean(&ppp_names);

    return 0;
}

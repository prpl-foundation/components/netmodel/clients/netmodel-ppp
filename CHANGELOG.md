# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]


## Release v0.2.1 - 2023-03-24(16:46:59 +0000)

### Fixes

- Fix netmodel-ppp unit tests

## Release v0.2.0 - 2022-08-29(15:11:53 +0000)

### New

- Dynamic handling of network events (PPP)

## Release v0.1.0 - 2022-05-23(19:44:53 +0000)

### New

- [netmodel][client] Add support for ppp clients in netmodel

